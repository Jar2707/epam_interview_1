package mainPackage;

public class Word {
	private static int number;
	private static char symbol;
	
	public void setNumber() {
		MenuInterface menu = new MenuInterface();
		Word.number = menu.inputNumber();
	}
	public void setSymbol() {
		MenuInterface menu = new MenuInterface();
		Word.symbol = menu.inputSymbol();
	}
	
	public String proceedWords (String[] words) {
		String sentence = "";
		for(String word : words) {
			word = replaceSymbol(word);
			sentence += (word + " ");
		}
		return sentence;
	}
	
	private String replaceSymbol(String word) {
		char[] wordModifiedAsCharArray;
		if(word.length() >= number && word.charAt(number - 1) != ','
				&& word.charAt(number - 1) != '.' && word.charAt(number - 1) != '!'
				&& word.charAt(number - 1) != '?' && word.charAt(number - 1) != ':'
				&& word.charAt(number - 1) != ';' && word.charAt(number - 1) != '('
				&& word.charAt(number - 1) != ')') {
			wordModifiedAsCharArray = word.toCharArray();
			wordModifiedAsCharArray[number - 1] = symbol;
		} else {
			wordModifiedAsCharArray = word.toCharArray();
		}
		String wordModified = new String(wordModifiedAsCharArray);
		return wordModified;
	}
}
