package mainPackage;

import java.util.Scanner;

public class MenuInterface {
	public String inputSentence() {
		String sentence;
		System.out.println("Input sentence:");
		Scanner sc = new Scanner(System.in);
		sentence = sc.nextLine();
		return sentence;
	}
	
	public int inputNumber() {
		int number = -1;
		System.out.println("Input non-negative number:");
		Scanner sc = new Scanner(System.in);
		while(number < 0) {
			if(sc.hasNextInt()) {
				number = sc.nextInt();
				if(number < 0) {
					System.out.println("Input non-negative number:");
					sc.nextLine();
				}
			} else {
				System.out.println("Input non-negative number:");
				sc.nextLine();
			}
		}
		return number;
	}
	
	public char inputSymbol() {
		char symbol = 0;
		System.out.println("Input symbol for replace:");
		Scanner sc = new Scanner(System.in);
		symbol = sc.nextLine().charAt(0);
		return symbol;
	}

}
