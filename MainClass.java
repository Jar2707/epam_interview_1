package mainPackage;

public class MainClass {
	public void inputSentence() {
		Sentence sentence = new Sentence();
		sentence.setSentencePrimary();
	}
	public void inputNumberAdnSymbol() {
		Word word = new Word();
		word.setNumber();
		word.setSymbol();
	}
	public void proceedSentenceAndShowResult() {
		Sentence sentence = new Sentence();
		Word word = new Word();
		String sentenceModified = null;
		sentenceModified = word.proceedWords(sentence.stringToWords());
		System.out.println(sentenceModified);
	}
	
	public static void main(String[] args) {
		MainClass mc = new MainClass();
		mc.inputSentence();
		mc.inputNumberAdnSymbol();
		mc.proceedSentenceAndShowResult();
	}
	
	
}
