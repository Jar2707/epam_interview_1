package mainPackage;

public class Sentence {
	private static String sentencePrimary;
	
	public void setSentencePrimary() {
		MenuInterface menu = new MenuInterface();
		Sentence.sentencePrimary = menu.inputSentence();
	}
		
	public String[] stringToWords() {
		String sentence = sentencePrimary;
		String[] words = sentence.split(" ");
		return words;
	}
}
